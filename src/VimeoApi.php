<?php
/**
 * @file
 * Contains \Drupal\vimeo_api\VimeoApi.
 */

namespace Drupal\vimeo_api;

use Vimeo\Vimeo;

class VimeoApi {

  /** @var \Drupal\Core\Config\ImmutableConfig */
  public $config;
  public $client_id;
  public $client_secret;
  public $access_token;

  /** @var Vimeo */
  public $api;

  public function __construct() {
    $this->config = \Drupal::config('vimeo_api.api');
    $this->setCredentials();
  }

  /**
   * Checks whether Vimeo API has been configured correctly.
   *
   * @param bool $display_messages
   *  Indicates whether to display messages when configuration missing.
   *
   * @return bool A boolean indicating whether all the required
   *  A boolean indicating whether all the required
   *  configuration variables have been set.
   */
  public function isConfigured($display_messages = FALSE) {
    if ($display_messages) {
      if (empty($this->client_id)) {
        drupal_set_message(t('Client ID for the Vimeo API is not available.'), 'error');
      }
      if (empty($this->client_secret)) {
        drupal_set_message(t('Client Secret for the Vimeo API is not available.'), 'error');
      }
      if (empty($this->access_token)) {
        drupal_set_message(t('Access token for the Vimeo API is not available.'), 'error');
      }
    }
    if (!empty($this->client_id) && !empty($this->client_secret) && !empty($this->access_token)) {
      return TRUE;
    }
    return FALSE;
  }

  public function setCredentials() {
    if ($this->config->get('client_id') && $this->config->get('client_secret')) {
      $this->client_id = $this->config->get('client_id');
      $this->client_secret = $this->config->get('client_secret');
      $this->api = new Vimeo($this->client_id, $this->client_secret);
      $this->setAccessToken();
    }
  }

  public function setAccessToken() {
    $this->access_token = NULL;
    $this->api->setToken(NULL);
    if ($this->config->get('access_token')) {
      $this->access_token = $this->config->get('access_token');
      $this->api->setToken($this->access_token);
    }
  }
}