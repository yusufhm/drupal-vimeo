<?php
/**
 * @file
 * Contains \Drupal\vimeo_api\Form\VimeoUploadForm.
 */

namespace Drupal\vimeo_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\vimeo_api\VimeoApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class VimeoUploadForm extends FormBase {

  /** @var VimeoApi */
  protected $vimeo_api;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vimeo_api.api')
    );
  }

  public function __construct(VimeoApi $vimeo_api) {
    $this->vimeo_api = $vimeo_api;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_api_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->vimeo_api->isConfigured(TRUE)) {
      $response = $this->vimeo_api->api->request('/me', array(), 'GET');

      $text = $response['body']['name'];
      $link = Link::fromTextAndUrl($text, Url::fromUri($response['body']['link'], array('attributes' => array('target' => '_blank'))));
      $form['account_url'] = array(
        '#type' => 'markup',
        '#markup' => $link->toString(),
        '#prefix' => '<div>Vimeo Account: ',
        '#suffix' => '</div>'
      );

      $form['url'] = array(
        '#type' => 'url',
        '#title' => t('Upload URL'),
        '#description' => t('Enter a URL to a video that you want to upload to Vimeo. Vimeo will automatically upload the video from that URL.'),
        '#required' => TRUE,
      );

      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Upload'),
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->vimeo_api->isConfigured(TRUE)) {
      $url = $form_state->getValue('url');
      $response = $this->vimeo_api->api->request('/me/videos', array('type' => 'pull', 'link' => $url), 'POST');
      if ($response['status'] == 200) {
        $text = $response['body']['name'];
        $link = Link::fromTextAndUrl($text, Url::fromUri($response['body']['link'], array('attributes' => array('target' => '_blank'))));
        drupal_set_message('Video created: ' . $link->toString());
      }
    }
  }
}