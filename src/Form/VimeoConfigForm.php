<?php

/**
 * @file
 * Contains \Drupal\vimeo_api\Form\VimeoConfigForm.
 */

namespace Drupal\vimeo_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\vimeo_api\VimeoApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vimeo\Vimeo;

/**
 * Configure the Vimeo API for this site.
 */
class VimeoConfigForm extends ConfigFormBase {

  protected $vimeo_api;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vimeo_api.api')
    );
  }

  public function __construct(ConfigFactoryInterface $config_factory, VimeoApi $vimeo_api) {
    parent::__construct($config_factory);
    $this->vimeo_api = $vimeo_api;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vimeo_api.api'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    global $base_url;
    $redirect_uri = $base_url . '/admin/config/services/vimeo-api';

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#default_value' => $this->vimeo_api->client_id,
      '#description' => t('The Vimeo Client ID. Can be obtained from <a href="https://developer.vimeo.com/apps" target="_blank">here</a>, clicking on the desired app and go to the "Authentication" tab.'),
      '#required' => TRUE,
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#default_value' => $this->vimeo_api->client_secret,
      '#description' => t('The Vimeo Client Secrets. Similarly as above, can be obtained from <a href="https://developer.vimeo.com/apps" target="_blank">here</a>, clicking on the desired app and go to the "Authentication" tab.'),
      '#required' => TRUE,
    );

    if ($this->vimeo_api->api) {
      // Check if we are in a callback.
      $request = \Drupal::request();
      $query_params = $request->query;
      // If we have the same 'state' from the callback
      // as well 'code' in the query params, Vimeo
      // replied positively to the auth URL call.
      $valid_state = ($query_params->has('state') && $query_params->get('state') == $config->get('auth_state'));
      if ($valid_state && $query_params->has('code')) {
        // Proceed to retrieving the access token
        $token = $this->vimeo_api->api->accessToken($query_params->get('code'), $redirect_uri);
        if (!empty($token['body']['access_token'])) {
          $config->set('access_token', $token['body']['access_token']);
          $config->clear('auth_state');
          $config->save();
          $this->vimeo_api->setAccessToken();
        }
      }
      // Otherwise generate the AUTH url.
      elseif ($this->vimeo_api->api && empty($this->vimeo_api->access_token)) {
        // Generate API params.
        $scopes = array('private', 'upload', 'create', 'edit', 'video_files');
        $uuid_service = \Drupal::service('uuid');
        $state = $uuid_service->generate();
        $config->set('auth_state', $state);
        $config->save();

        // Generate the Authorize URL.
        $authorize_url = $this->vimeo_api->api->buildAuthorizationEndpoint($redirect_uri, $scopes, $state);

        $text = 'Authorisation URL';
        $link = Link::fromTextAndUrl($text, Url::fromUri($authorize_url));
        $form['auth_url'] = array(
          '#type' => 'markup',
          '#markup' => $link->toString(),
        );
      }
      // Request a new access token.
      elseif ($this->vimeo_api->api) {
        $form['actions']['new_token'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Request new access token'),
        );
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('vimeo_api.api');
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#parents'][0] == "new_token") {
      $config->clear('access_token');
      $config->save();
      $this->vimeo_api->setAccessToken();
    }
    else {
      $config->set('client_id', $form_state->getValue('client_id'));
      $config->set('client_secret', $form_state->getValue('client_secret'));
      $config->save();
      $this->vimeo_api->setCredentials();
      parent::submitForm($form, $form_state);
    }
  }
}